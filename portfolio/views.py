# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.core import serializers

from portfolio.models import CaseStudy, Comment
from portfolio.forms import CommentForm

def index(request):
    studies = CaseStudy.objects.all()
    context = {'studies' : studies}
    return render(request, 'portfolio/index.html', context)

def comment_success(request, study_id):
    study = get_object_or_404(CaseStudy, pk=study_id)

    context = {'study': study}
    return render(request, 'portfolio/comment_thankyou.html', context)


def detail(request, study_id):
    study = get_object_or_404(CaseStudy, pk=study_id)
    
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            cd = comment_form.cleaned_data
            c = Comment(message=cd.get('message'), author=cd.get('author'), casestudy=study)
            c.save()

            context = {
                'study': study,
                'form': comment_form,
            }
            return HttpResponseRedirect(reverse('portfolio:comment-success', args=(study_id,)))
        else:
            context = {
                'study': study,
                'form': comment_form,
                'error': 'Error with your form. Please fix.',
            }
            return render(request, 'portfolio/detail.html', context)
            

    comment_form = CommentForm()
    context = {
        'study' : study,
        'form' : comment_form,
    }
    return render(request, 'portfolio/detail.html', context)

def detail_ajax(request, study_id):
    study = get_object_or_404(CaseStudy, pk=study_id)
    
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            cd = comment_form.cleaned_data
            c = Comment(message=cd.get('message'), author=cd.get('author'), casestudy=study)
            c.save()

            context = {
                'study': study,
                'form': comment_form,
            }
            return HttpResponseRedirect(reverse('portfolio:comment-success', args=(study_id,)))
        else:
            context = {
                'study': study,
                'form': comment_form,
                'error': 'Error with your form. Please fix.',
            }
            return render(request, 'portfolio/detail_ajax.html', context)
            

    comment_form = CommentForm()
    context = {
        'study' : study,
        'form' : comment_form,
    }
    return render(request, 'portfolio/detail_ajax.html', context)

def get_comments_ajax(request, study_id):
    study = get_object_or_404(CaseStudy, pk=study_id)

    comments = serializers.serialize('json', Comment.objects.filter(casestudy=study), fields=('message', 'author', 'pub_date',))

    return HttpResponse(comments)
