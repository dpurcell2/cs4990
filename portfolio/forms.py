from django import forms

class CommentForm(forms.Form):
    message = forms.CharField(max_length=200)
    author = forms.CharField(max_length=200)
