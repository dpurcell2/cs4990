from django.conf.urls import patterns, url

from portfolio import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<study_id>\d+)/$', views.detail, name='detail'),
    url(r'^(?P<study_id>\d+)/ajax/$', views.detail_ajax, name='detail-ajax'),
    url(r'^(?P<study_id>\d+)/thankyou/$', views.comment_success, name='comment-success'),
    url(r'^(?P<study_id>\d+)/comments/$', views.get_comments_ajax, name='get-comments'),
)

