from django.contrib import admin
from portfolio.models import CaseStudy, Comment

class CaseStudyAdmin(admin.ModelAdmin):
    search_fields = ['title', 'description']
    list_display = ('title', 'description', )

class CommentAdmin(admin.ModelAdmin):
    list_display = ('message', 'author', 'pub_date')

admin.site.register(CaseStudy, CaseStudyAdmin)
admin.site.register(Comment, CommentAdmin)
